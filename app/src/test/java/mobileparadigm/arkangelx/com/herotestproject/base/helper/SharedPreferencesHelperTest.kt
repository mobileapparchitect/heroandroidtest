package com.base.helper

import android.content.SharedPreferences
import com.google.gson.Gson
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.helper.SharedPreferencesHelper
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mockito.`when`


class SharedPreferencesHelperTest {

    private val sharedPreferences: SharedPreferences = mock()
    private val gson: Gson = mock()
    private lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private val key = "testKey"
    private val editor: SharedPreferences.Editor = mock()

    @Before
    fun setUp(){
        `when`(sharedPreferences.edit()).thenReturn(editor)
        sharedPreferencesHelper = SharedPreferencesHelper(sharedPreferences, gson)
    }

    @Test
    fun getDataFromSharedPref_should_returnInt() {
        val result = sharedPreferencesHelper.getDataFromSharedPref(Int::class.java, key)
        verify(sharedPreferences).getInt(key, 0)
        assertEquals(result, 0)
    }

    @Test
    fun getDataFromSharedPref_should_returnString() {
        val result = sharedPreferencesHelper.getDataFromSharedPref(String::class.java, key)
        verify(sharedPreferences).getString(key, null)
        assertEquals(result, null)
    }

    @Test
    fun getDataFromSharedPref_should_returnLong() {
        val result = sharedPreferencesHelper.getDataFromSharedPref(Long::class.java, key)
        verify(sharedPreferences).getLong(key, 0)
        assertEquals(result, 0L)
    }

    @Test
    fun getDataFromSharedPref_should_returnBool() {
        val result = sharedPreferencesHelper.getDataFromSharedPref(Boolean::class.java, key)
        verify(sharedPreferences).getBoolean(key, false)
        assertEquals(result, false)
    }

    @Test
    fun getDataFromSharedPref_should_returnNull() {
        val result = sharedPreferencesHelper.getDataFromSharedPref(Double::class.java, key)
        assertEquals(result, null)
    }

    @Test
    fun putDatatoSharedPref_should_putInt() {
        sharedPreferencesHelper.putDatatoSharedPref(0, Int::class.java, key)
        verify(editor).putInt(key, 0)
        verify(editor).commit()
    }

    @Test
    fun putDatatoSharedPref_should_putString() {
        sharedPreferencesHelper.putDatatoSharedPref("testString", String::class.java, key)
        verify(editor).putString(key, "testString")
        verify(editor).commit()
    }

    @Test
    fun putDatatoSharedPref_should_putLong() {
        sharedPreferencesHelper.putDatatoSharedPref(10L, Long::class.java, key)
        verify(editor).putLong(key, 10L)
        verify(editor).commit()
    }

    @Test
    fun putDatatoSharedPref_should_putBool() {
        sharedPreferencesHelper.putDatatoSharedPref(true, Boolean::class.java, key)
        verify(editor).putBoolean(key, true)
        verify(editor).commit()
    }

}