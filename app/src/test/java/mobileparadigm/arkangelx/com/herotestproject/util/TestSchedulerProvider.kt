package com.util

import io.reactivex.Scheduler
import io.reactivex.schedulers.TestScheduler
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.util.SchedulerProvider


class TestSchedulerProvider constructor(private val testScheduler: TestScheduler) : SchedulerProvider {
    override fun ui(): Scheduler = testScheduler
    override fun computation(): Scheduler = testScheduler
    override fun io(): Scheduler = testScheduler
}