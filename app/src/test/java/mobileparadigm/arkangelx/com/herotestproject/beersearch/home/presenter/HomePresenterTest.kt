package mobileparadigm.arkangelx.com.herotestproject.beersearch.home.presenter

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.util.TestSchedulerProvider
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.TestScheduler
import mobileparadigm.arkangelx.com.herotestproject.beersearch.api.Endpoints
import mobileparadigm.arkangelx.com.herotestproject.beersearch.home.model.SearchResponse
import org.junit.After
import org.junit.Before
import org.junit.Test

class HomePresenterTest {

    private val view: HomeView = mock()
    private val api: Endpoints = mock()
    private lateinit var presenter: HomePresenter
    private lateinit var testScheduler: TestScheduler

    @Before
    fun setup() {
        val compositeDisposable = CompositeDisposable()
        testScheduler = TestScheduler()
        val testSchedulerProvider = TestSchedulerProvider(testScheduler)
        presenter = HomePresenter(api, compositeDisposable, testSchedulerProvider)
        presenter.attachView(view)
    }




    @Test
    fun test_initateSearchForBeer() {
        val mockedResponse: SearchResponse = mock()
        val searchKey = "test"

        doReturn(Observable.just(mockedResponse))
                .`when`(api)
                .searchForBeer(searchKey)

        presenter.initateSearchForBeer(searchKey)

        testScheduler.triggerActions()

        verify(view).showProgress()
        verify(view).onSearchComplete(mockedResponse)
        verify(view).hideProgress()
    }

    @Test
    fun test_initateSearchForBrewery() {
        val mockedResponse: SearchResponse = mock()
        val searchKey = "test"

        doReturn(Observable.just(mockedResponse))
                .`when`(api)
                .searchForBrewery(searchKey)

        presenter.initateSearchForBrewery(searchKey)

        testScheduler.triggerActions()

        verify(view).showProgress()
        verify(view).onSearchComplete(mockedResponse)
        verify(view).hideProgress()
    }

    @After
    fun tearDown() {
        presenter.detachView()
    }
}