package mobileparadigm.arkangelx.com.herotestproject.beersearch.home.presenter

import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.mvp.BaseView
import mobileparadigm.arkangelx.com.herotestproject.beersearch.home.model.SearchResponse

interface HomeView : BaseView {
    fun onSearchComplete(searchResponse: SearchResponse?)
    fun onSearchBreweryComplete(searchResponse: SearchResponse?)
    fun showProgress()
    fun hideProgress()
    fun noResult()
}
