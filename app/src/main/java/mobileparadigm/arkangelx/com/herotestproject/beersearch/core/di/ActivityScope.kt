package mobileparadigm.arkangelx.com.herotestproject.beersearch.core.di

import javax.inject.Scope

@Scope
@Retention
annotation class ActivityScope