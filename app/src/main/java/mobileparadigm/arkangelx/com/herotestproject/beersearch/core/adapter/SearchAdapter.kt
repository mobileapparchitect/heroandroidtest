package mobileparadigm.arkangelx.com.herotestproject.beersearch.core.adapter

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import mobileparadigm.arkangelx.com.herotestproject.R
import mobileparadigm.arkangelx.com.herotestproject.beersearch.home.model.SearchResponse
import org.greenrobot.eventbus.EventBus

class SearchAdapter() : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
    private var itemList: ArrayList<SearchResponse?>? = null

    init {
        itemList = ArrayList()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.search_result_items, parent, false)
        return ViewHolder(v);
    }

    fun addItems(baseResponseList: List<SearchResponse>) {
        itemList?.addAll(baseResponseList)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return itemList?.size!!
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val itemAtPosition = itemList!!.get(position)!!
        holder?.itemName?.text = itemAtPosition.title
        holder?.rootView?.setOnClickListener({ _ -> EventBus.getDefault().post(itemAtPosition) })

    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemName = itemView.findViewById<TextView>(R.id.itemName)
        val txtTitle = itemView.findViewById<TextView>(R.id.itemDescription)
        val rootView = itemView.findViewById<CardView>(R.id.root)

    }
}