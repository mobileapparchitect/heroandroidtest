package mobileparadigm.arkangelx.com.herotestproject.beersearch.home.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class SearchResponse {

    @SerializedName("key")
    @Expose
    var key: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("synonyms")
    @Expose
    var synonyms: String? = null
    @SerializedName("abv")
    @Expose
    var abv: String? = null
    @SerializedName("srm")
    @Expose
    var srm: Any? = null
    @SerializedName("og")
    @Expose
    var og: Any? = null
    @SerializedName("tags")
    @Expose
    var tags: List<String>? = null

    @SerializedName("country")
    @Expose
    var country: Country? = null


}
