package mobileparadigm.arkangelx.com.herotestproject.beersearch.core.mvp


interface Presenter<V : BaseView> {

    fun attachView(view: V)

    fun detachView()
}