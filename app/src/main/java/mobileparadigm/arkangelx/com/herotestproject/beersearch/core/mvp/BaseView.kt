package mobileparadigm.arkangelx.com.herotestproject.beersearch.core.mvp


interface BaseView {
    fun onError()
    fun setPresenter(presenter: BasePresenter<*>)
}