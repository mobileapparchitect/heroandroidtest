package mobileparadigm.arkangelx.com.herotestproject.beersearch.core

import android.app.Application

import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.di.component.AppComponent
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.di.component.DaggerAppComponent
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.di.module.AppModule


class App : Application() {

    companion object {
        @JvmStatic
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDagger()

    }

    private fun initDagger() {
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }

}