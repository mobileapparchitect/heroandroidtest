package mobileparadigm.arkangelx.com.herotestproject.beersearch.home.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Country {

    @SerializedName("key")
    @Expose
    var key: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null

}
