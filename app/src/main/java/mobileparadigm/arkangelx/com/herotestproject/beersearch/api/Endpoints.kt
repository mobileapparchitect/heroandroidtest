package mobileparadigm.arkangelx.com.herotestproject.beersearch.api

import io.reactivex.Observable
import mobileparadigm.arkangelx.com.herotestproject.beersearch.home.model.SearchResponse
import retrofit2.http.GET
import retrofit2.http.Path


interface Endpoints {
    @GET("v1/beer/{beer}/")
    fun searchForBeer(@Path("beer") beer: String): Observable<SearchResponse>

    @GET("v1/brewery/{brewery}/")
    fun searchForBrewery(@Path("brewery") brewery: String): Observable<SearchResponse>
}