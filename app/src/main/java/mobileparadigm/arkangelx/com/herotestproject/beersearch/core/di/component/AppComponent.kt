package mobileparadigm.arkangelx.com.herotestproject.beersearch.core.di.component

import android.app.Application
import android.content.res.Resources
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.di.module.ApiModule
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.di.module.AppModule
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.di.module.OkHttpModule
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.di.module.RetrofitModule
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.helper.SharedPreferencesHelper
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.util.AppSchedulerProvider
import mobileparadigm.arkangelx.com.herotestproject.beersearch.api.Endpoints
import com.google.gson.Gson
import dagger.Component
import io.reactivex.disposables.CompositeDisposable
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(AppModule::class, RetrofitModule::class, ApiModule::class, OkHttpModule::class))
interface AppComponent {
    fun application(): Application
    fun gson(): Gson
    fun resources(): Resources
    fun retrofit():Retrofit
    fun endpoints(): Endpoints
    fun cache(): Cache
    fun client(): OkHttpClient
    fun loggingInterceptor(): HttpLoggingInterceptor
    fun spHelper(): SharedPreferencesHelper
    fun compositeDisposable(): CompositeDisposable
    fun schedulerProvider(): AppSchedulerProvider
}