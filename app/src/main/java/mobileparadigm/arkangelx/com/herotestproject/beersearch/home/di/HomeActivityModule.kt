package mobileparadigm.arkangelx.com.herotestproject.beersearch.home.di

import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.di.ActivityScope
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.util.AppSchedulerProvider
import mobileparadigm.arkangelx.com.herotestproject.beersearch.api.Endpoints
import mobileparadigm.arkangelx.com.herotestproject.beersearch.home.presenter.HomePresenter
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
class HomeActivityModule {

    @Provides
    @ActivityScope
    internal fun providesHomePresenter(api: Endpoints, disposable: CompositeDisposable, scheduler: AppSchedulerProvider): HomePresenter =
            HomePresenter(api, disposable, scheduler)
}
