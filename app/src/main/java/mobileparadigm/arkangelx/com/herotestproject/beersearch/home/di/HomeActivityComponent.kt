package mobileparadigm.arkangelx.com.herotestproject.beersearch.home.di

import dagger.Component
import mobileparadigm.arkangelx.com.herotestproject.beersearch.home.HomeActivity
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.di.ActivityScope
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.di.component.AppComponent

@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(HomeActivityModule::class))
interface HomeActivityComponent {

    fun inject(mainActivity: HomeActivity)
}
