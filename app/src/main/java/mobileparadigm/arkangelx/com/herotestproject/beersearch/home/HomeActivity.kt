package mobileparadigm.arkangelx.com.herotestproject.beersearch.home

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import com.mancj.materialsearchbar.MaterialSearchBar
import kotlinx.android.synthetic.main.activity_main.*
import mobileparadigm.arkangelx.com.herotestproject.R
import mobileparadigm.arkangelx.com.herotestproject.R.id.homeRv
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.BaseActivity
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.adapter.SearchAdapter
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.helper.SharedPreferencesHelper
import mobileparadigm.arkangelx.com.herotestproject.beersearch.home.di.DaggerHomeActivityComponent
import mobileparadigm.arkangelx.com.herotestproject.beersearch.home.di.HomeActivityModule
import mobileparadigm.arkangelx.com.herotestproject.beersearch.home.model.SearchResponse
import mobileparadigm.arkangelx.com.herotestproject.beersearch.home.presenter.HomePresenter
import mobileparadigm.arkangelx.com.herotestproject.beersearch.home.presenter.HomeView
import org.greenrobot.eventbus.Subscribe
import org.jetbrains.anko.toast
import javax.inject.Inject


class HomeActivity : BaseActivity(), HomeView, MaterialSearchBar.OnSearchActionListener {


    @Inject
    lateinit var presenter: HomePresenter

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper

    private lateinit var searchAdapter: SearchAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initAdapter()
        initSearchView()
    }

    private fun initAdapter() {

        with(homeRv) {
            homeRv.layoutManager = LinearLayoutManager(this@HomeActivity, LinearLayout.VERTICAL, false)
            searchAdapter = SearchAdapter()
            homeRv.adapter = searchAdapter
        }

    }


    override fun onSearchComplete(searchResponse: SearchResponse?) {
        searchAdapter.addItems(listOf<SearchResponse>(searchResponse!!))
    }

    override fun onSearchBreweryComplete(searchResponse: SearchResponse?) {

    }

    private fun initSearchView() {
        with(homeSearchView) {
            setHint(getString(R.string.searchview_hint))
            setSpeechMode(true)
            setOnSearchActionListener(this@HomeActivity)
            val suggestions = sharedPreferencesHelper.getSuggestions()
            if (suggestions != null) {
                lastSuggestions = suggestions
            }
        }
    }

    override fun onActivityInject() {
        DaggerHomeActivityComponent.builder().appComponent(getAppcomponent())
                .homeActivityModule(HomeActivityModule())
                .build()
                .inject(this)

        presenter.attachView(this)
    }


    override fun showProgress() {
        homeRv.visibility = View.GONE
        homeShimmer.visibility = View.VISIBLE
        homeLogo.visibility = View.GONE
        homeShimmer.startShimmerAnimation()
    }

    override fun hideProgress() {
        homeRv.visibility = View.VISIBLE
        homeShimmer.visibility = View.GONE
        homeShimmer.stopShimmerAnimation()
    }

    override fun onButtonClicked(buttonCode: Int) {}

    override fun onSearchStateChanged(enabled: Boolean) {}

    override fun onSearchConfirmed(text: CharSequence?) {
        presenter.initateSearchForBeer(text?.toString() ?: "")
    }

    override fun noResult() {
        homeLogo.visibility = View.VISIBLE
        toast("We couldn't find any search query results.")
    }

    override fun onDestroy() {
        super.onDestroy()
        sharedPreferencesHelper.setSuggestions(homeSearchView.lastSuggestions)
    }

    @Subscribe
    fun onRowClicked(item: SearchResponse) {
        toast("Brewery Name's name: ${item.title}")
    }

}
