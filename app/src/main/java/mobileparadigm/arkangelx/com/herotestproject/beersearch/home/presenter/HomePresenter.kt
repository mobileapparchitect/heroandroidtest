package mobileparadigm.arkangelx.com.herotestproject.beersearch.home.presenter

import io.reactivex.disposables.CompositeDisposable
import mobileparadigm.arkangelx.com.herotestproject.beersearch.api.Endpoints
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.mvp.BasePresenter
import mobileparadigm.arkangelx.com.herotestproject.beersearch.core.util.SchedulerProvider
import javax.inject.Inject

class HomePresenter @Inject constructor(var api: Endpoints, disposable: CompositeDisposable, scheduler: SchedulerProvider) : BasePresenter<HomeView>(disposable, scheduler) {



    fun initateSearchForBeer(searchKey: String) {
        view?.showProgress()
        disposable.add(api.searchForBeer(searchKey)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())

                .subscribe(
                        { result ->
                            view?.hideProgress()
                            view?.onSearchComplete(result)
                            initateSearchForBrewery(result.title!!)

                            if (result == null) {
                                view?.noResult()
                            }
                        },
                        { _ ->
                            view?.hideProgress()
                            view?.onError()
                        })

        )
    }


    fun initateSearchForBrewery(searchKey: String) {
        view?.showProgress()
        disposable.add(api.searchForBrewery(searchKey)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())

                .subscribe(
                        { result ->
                            view?.hideProgress()
                            view?.onSearchComplete(result)

                            if (result == null) {
                                view?.noResult()
                            }
                        },
                        { _ ->
                            view?.hideProgress()
                            view?.onError()
                        })

        )
    }
}